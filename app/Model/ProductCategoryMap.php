<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategoryMap extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'product_category_map';

    public function cat()
    {
        return $this->belongsTo('App\Model\Category', 'cat_id', 'id');
    }
}
