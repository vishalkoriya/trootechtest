<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Model\ProductCategoryMap;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function create(Request $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->save();

        foreach ($request->categories as $key => $value) {
            $proCat = new ProductCategoryMap();
            $proCat->product_id = $product->id;
            $proCat->cat_id = $value;
            $proCat->save();
        }

        return Product::with(['category' => function ($q) {
            $q->with('cat');
        }])->where('id', $product->id)->first();

    }

    public function get()
    {

        return Product::with(['category' => function ($q) {
            $q->with('cat');
        }])->paginate(10);

    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->save();

        //Delete old category
        ProductCategoryMap::where('product_id', $product->id)->delete();

        foreach ($request->categories as $key => $value) {
            $proCat = new ProductCategoryMap();
            $proCat->product_id = $product->id;
            $proCat->cat_id = $value;
            $proCat->save();
        }
        return Product::with(['category' => function ($q) {
            $q->with('cat');
        }])->where('id', $product->id)->first();

    }

    public function delete($id)
    {
        $product = Product::find($id);
        $is_delete = $product->delete();

        $is_delete_cat = ProductCategoryMap::where('product_id', $product->id)->delete();

        if ($is_delete && $is_delete_cat) {
            return response()->json(['Deleted Successsfully']);
        }
    }
}
