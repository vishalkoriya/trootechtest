<?php

namespace App\Http\Controllers;

use App\Model\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function create(Request $request)
    {

        $cat = new Category();
        $cat->name = $request->name;

        if ($request->parent_id) {
            $cat->parent_id = $request->parent_id;
        }
        $cat->save();

        return $cat;
    }

    public function get()
    {

        $cat = Category::whereNull('parent_id')->get();

        foreach ($cat as $key => $value) {
            $cat[$key]['child'] = $this->getSubCat($value->id);
        }
        return $cat;
    }

    public function delete($id)
    {
        $cat = Category::find($id);
        $is_del = $cat->delete();

        if ($is_del) {
            return response()->json(['deleted successfully']);
        }
    }
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->name;
        $category->parent_id = isset($request->parent_id) ? $request->parent_id : null;
        $category->save();

        return $category;
    }

    public function getSubCat($parent_id)
    {

        $child = Category::where('parent_id', $parent_id)
            ->get();

        foreach ($child as $key => $value) {

            $hasChild = Category::where('parent_id', $value->id)->get();

            if (count($hasChild)) {
                $child[$key]['child'] = $this->getSubCat($value->id);
            } else {
                continue;
            }
        }

        return $child;

    }

}
