<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Category
Route::post('/createCat', [CategoryController::class, 'create']);
Route::get('/cat', [CategoryController::class, 'get']);
Route::put('/updateCat/{id}', [CategoryController::class, 'update']);
Route::delete('/deleteCat/{id}', [CategoryController::class, 'delete']);

//Product
Route::post('/createPro', [ProductController::class, 'create']);
Route::get('/product', [ProductController::class, 'get']);
Route::put('/updatePro/{id}', [ProductController::class, 'update']);
Route::delete('/deletePro/{id}', [ProductController::class, 'delete']);
